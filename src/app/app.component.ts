import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { appState } from 'src/actions/states';
import { carditemSelector } from 'src/reducers/reducer';
import { GlobalService } from './providers/global.service';
import { HttpProviderService } from './providers/http-provider.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public cardInfo


   constructor(public gs : GlobalService , public router : Router , public ar : ActivatedRoute , 
     private store : Store<appState>){
      // this.getInfo()
      // this.ar.params.subscribe(para =>{
      //   console.log('called')
      //   if(!this.router.url.includes('payment')){
      //     setInterval(() =>{
      //       this.getInfo()
      //     } , 1000)
          
      //   }
      // })

      this.store.select(carditemSelector).subscribe(data =>{
        this.cardInfo = data
         console.log(data)
       })
   }

   ngOnInit(): void {
     //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
     //Add 'implements OnInit' to the class.
   }


  //  async getInfo(){
  //    this.store.select(carditemSelector).subscribe(data =>{
  //     this.cardInfo = data
  //     //  console.log(data)
  //    })
  //  }
}
