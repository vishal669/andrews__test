import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClearInputComponent } from './components/clear-input/clear-input.component';
import { InputErrorComponent } from './components/input-error/input-error.component';
import { ToastyModule } from 'ng2-toasty';



@NgModule({
  declarations: [ClearInputComponent, InputErrorComponent],

  exports: [

  ClearInputComponent,
  InputErrorComponent,
  ToastyModule
],

  
  imports: [
    CommonModule,
    ToastyModule.forRoot()
  ]
})
export class SharedModule { }
