import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addCardDetails } from 'src/actions/actions';
import { GlobalService } from '../providers/global.service';
import { HttpProviderService } from '../providers/http-provider.service';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit {
  isProcessing: boolean = false;
  public cardDetailForm: FormGroup;
  date = new Date()
  month = this.date.getMonth()
  currentMonth = ("0" + (this.month + 1)).slice(-2)
  currentYear = this.date.getFullYear()
  constructor(public fb: FormBuilder, public gs: GlobalService,
    public hps: HttpProviderService, private store: Store) { }


  ngOnInit(): void {
    this.initializaForm()
  }

  initializaForm() {
    this.cardDetailForm = this.fb.group({
      cardNumber: ['', [Validators.required]],
      cardHolderName: ['', [Validators.required]],
      expiryDate: ['', [Validators.required]],
      cvvCode: ['', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]],
      amount: [null, [Validators.required]]
    });
  }


  async sendDetails() {
    console.log(this.cardDetailForm)

    if (this.cardDetailForm.invalid) {
      Object.keys(this.cardDetailForm.value).forEach((value) => {

        this.cardDetailForm['controls'][value].markAsDirty();
        this.cardDetailForm['controls'][value].markAsTouched();

      })
    }

    this.isProcessing = true

    try {
      let responce = await this.hps.postCall(this.cardDetailForm.value)
      console.log(responce)
      this.store.dispatch(new addCardDetails(responce))
      this.gs.showToast('success', 'Card Details Added', 'Successfully')
      this.cardDetailForm.reset()
      this.isProcessing = false

    } catch (error) {
      console.log(error)
      this.gs.showToast('error', error.message)
      this.isProcessing = false

    }

  }




}
