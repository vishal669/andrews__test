import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public dataDispatched = new Subject<any>()

  constructor(public router : Router ,  private toastyService: ToastyService,
    private toastyConfig: ToastyConfig){
      this.toastyConfig.theme = 'default';

  }

  public navigate(path:string) {
    // console.log(path)
    this.router.navigate([path]);

  }

  showToast(type: 'error' | 'info' | 'success' | 'warning', title: string, message?: string, data?) {
    
    this.toastyService[type]({
      msg: message,
      title: title,
    });
    
  }
}
