import { Injectable } from '@angular/core';
import { HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpProviderService {

  apiUrl = 'https://jsonplaceholder.typicode.com/posts'

  constructor(public http: HttpClient) { }


  async postCall(body: Object , token?) {
    return this.http.post(this.apiUrl, body, { observe: 'response' }).toPromise().then((response: HttpResponse<any>) => {
      if (response.status === 200 || response.status === 201) {
        //console.log(response.body.content);
        return Promise.resolve( response.body.content || response.body.count ||  response.body);
      }
    }).catch((error: HttpErrorResponse) => {
      return Promise.reject(error['content'] || error);
    })
  }
}
