


export interface card {
    cardNumber : string,
    cardHolderName : string,
    expiryDate : string,
    cvvCode : string,
    amount : number
}


export interface cardItems {
    cardItem : card[]
}