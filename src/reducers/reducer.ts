import { Action, ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";
import { addAction, addCardDetails } from "src/actions/actions";
import { appState } from "src/actions/states";
import { card, cardItems } from "src/models/cardInterface";



export interface cardState {
    cardNumber : string,
    cardHolderName : string,
    expiryDate : string,
    cvvCode : string,
    amount : number
}

export const cardFormInitialState : cardItems = {
    cardItem : []
};

export function reducer(state : cardItems = cardFormInitialState , action : addCardDetails) : cardItems{
    
    switch(action.type){
        case addAction : {
        console.log(state.cardItem , action.payload)
            
            return Object.assign({} , state , {
                
                cardItem : [ ...state.cardItem , action.payload]
            } )
        }
        default : {
            return state
        }
    }
}



export const getUserCardInfo = ( state : card) => state;

// export const getUserCardState = ( state : reducerState ) => state;

export const cardItem = createFeatureSelector<appState , cardItems>('item')


export const carditemSelector = createSelector(cardItem , 
    (item: cardItems) =>{
        return item
    } )


export const reducers : ActionReducerMap<appState> = {
    item : reducer
}