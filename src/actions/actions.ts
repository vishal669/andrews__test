import { Action } from "@ngrx/store";


export const addAction = 'user added card details'

export class addCardDetails implements Action {
    readonly type = addAction

    constructor(public payload: {
        cardNumber : string;
        cardHolderName : string;
        expiryDate : string;
        cvvCode : string;
        amount : number
    }) {
      
    }
}
